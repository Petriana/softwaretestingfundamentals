package calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.lang.Integer.MAX_VALUE;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TestParametrized {
//    @Parameters
//    public static Collection<Integer[]> parameters() {
//        return Arrays.asList(new Integer[][] {
//                {1, 1, 2},
//                {2, 4, 6},
//                {1, 6, 7},
//                {4, 1, 5}
//        });
//    }
//    @Parameter(0)
//    public int a;
//
//    @Parameter(1)
//    public int b;
//
//    @Parameter(2)
//    public int expectedResult;
//
//    @Test
//    public void testAdd(){
//        //given
//        Calculator calculator = new Calculator();
//        //when
//        int actualResult = calculator.add(a,b);
//        //then
//        assertEquals(expectedResult,actualResult);
//    }

//    @Parameters
//    public static Collection<Integer[]> parameters1() {
//        return Arrays.asList(new Integer[][] {
//                {4, 2, 2},
//                {10, 5, 2},
//                {15, 3, 5},
//                {7, 2, 5}
//        });
//    }
//    @Parameter(0)
//    public int i;
//
//    @Parameter(1)
//    public int j;
//
//    @Parameter(2)
//    public int expectedResult1;
//
//    @Test
//    public void testDivide() {
//        //given
//        Calculator calculator = new Calculator();
//        //when
//        int actualResult1 = calculator.divide(i,j);
//        //then
//        assertEquals(expectedResult1,actualResult1);
//
//    }

        @Parameters
    public static Collection<Integer[]> parameters2() {
        return Arrays.asList(new Integer[][] {
                {-1, 2, 1},
                {MAX_VALUE, -2, 3},

        });
    }
    @Parameter(0)
    public int m;

    @Parameter(1)
    public int n;

    @Parameter(2)
    public int expectedResult2;
    @Test
    public void testDivide() {
        //given
        Calculator calculator = new Calculator();
        //when
        int actualResult1 = calculator.divide(m,n);
        //then
        assertEquals(expectedResult2,actualResult1);

    }

    }


