package calculator;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CalculatorTest {
    private Calculator calculator = new Calculator();
@BeforeClass
public static void setup() {
    System.out.println("Before each test");
}
@AfterClass
public static void tearDown(){
        System.out.println("After each test");
    }

    @Test
    public void test_add() {
        //given

        //when
        int result1 = calculator.add(4,4);
        //then
       // assert result == 8;

  //assertEquals(8,result1);
        int result2 = calculator.add(-1,-3);
        assertNotNull(result2);
    }

    @Test (expected = ArithmeticException.class)

    public void test_divide_success (){
        //given

        //when
        int result = calculator.add(4,4);
        //then
        assertNotNull(result);
        assertEquals("Divide operation works as expected",8,result);
        int result2 = calculator.divide(4,0);
        assertNotNull(result2);
    }

    @Test (expected = ArithmeticException.class)
  public void test_divide_throws_exception(){

      int result2 = calculator.divide(4,0);
      assertNotNull(result2);
  }

  @Test
    public void test_powerOfN_success(){

        int result = calculator.powerOfN(10,2);
        assertEquals(100,result);
  }

}
